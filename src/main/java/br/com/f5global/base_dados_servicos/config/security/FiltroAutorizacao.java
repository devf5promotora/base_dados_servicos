package br.com.f5global.base_dados_servicos.config.security;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.f5global.base_dados_servicos.config.SwaggerConfig;
import br.com.f5global.base_dados_servicos.service.dto.PayloadJwtDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FiltroAutorizacao extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    try {
      String token = this.recuperarToken(request);
      //      if (!Autorizacao.validToken(token.substring(7, token.length()))) {
      //        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Token Inválido!");
      //      }

      PayloadJwtDTO dadoToken = PayloadJwtDTO.recuperarUsuarioPorToken(token);
      UsernamePasswordAuthenticationToken auth =
          new UsernamePasswordAuthenticationToken(
              dadoToken,
              null,
              dadoToken.getAuthorities().stream()
                  .map(SimpleGrantedAuthority::new)
                  .collect(Collectors.toList()));

      SecurityContextHolder.getContext().setAuthentication(auth);
    } catch (Exception e) {
      log.error(request.getRequestURI() + " -- " + e.getMessage());
      SecurityContextHolder.clearContext();
    }

    filterChain.doFilter(request, response);
  }

  public String recuperarToken(HttpServletRequest request) {
    String token = request.getHeader(Autorizacao.HEADER_STRING);
    if (token == null || token.isEmpty() || !token.startsWith(Autorizacao.TOKEN_PREFIX)) {
      return null;
    }
    return token;
  }

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
    if (SwaggerConfig.PATHS.contains(request.getRequestURI())) {
      return true;
    }
    return false;
  }
}
