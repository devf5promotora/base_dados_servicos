package br.com.f5global.base_dados_servicos.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import br.com.f5global.base_dados_servicos.config.SwaggerConfig;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired private FiltroAutorizacao autorizacao;

  @Autowired private PontoDeEntradaDeAutenticacaoPersonalizado pontoEntrada;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.httpBasic().disable();

    http.csrf()
        .disable()
        .authorizeRequests()
        .antMatchers("/api/v1/base-clientes-fgts").permitAll()
        .antMatchers("/api/v1/base-clientes-fgts/{uuidBaseClientesFGTS}").permitAll()
        .antMatchers("/api/v1/base-clientes-fgts/{uuidBaseClientesFGTS}/alterar-cliente").permitAll()
        .antMatchers("/api/v1/base-clientes-fgts/exportar-csv").permitAll()
        .antMatchers("/api/v1/base-clientes-fgts/exportar-csv/gerenciador-arquivos").permitAll()
        .antMatchers("/api/v1/pre-cadastro-marketplace").permitAll()
        .antMatchers("/api/v1/pre-cadastro-marketplace/{uuidBaseClientesFGTS}").permitAll()
        .antMatchers("/api/v1/pre-cadastro-marketplace/{uuidBaseClientesFGTS}/alterar-parceiro").permitAll()
        .antMatchers("/api/v1/pre-cadastro-marketplace/exportar-csv").permitAll()
        .antMatchers("/api/v1/pre-cadastro-marketplace/exportar-csv/gerenciador-arquivos").permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .addFilterBefore(autorizacao, BasicAuthenticationFilter.class)
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(pontoEntrada);

    http.headers().frameOptions().sameOrigin().cacheControl();
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    
    for (String path : SwaggerConfig.PATHS) {
      web.ignoring().antMatchers(path);
    }
  }
}
