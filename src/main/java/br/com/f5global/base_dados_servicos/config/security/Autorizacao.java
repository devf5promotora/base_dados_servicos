package br.com.f5global.base_dados_servicos.config.security;

import io.jsonwebtoken.Jwts;

public class Autorizacao {

  static final Long EXPIRATION_TIME = 86400000L;
  static final String SECRET = "Ux8pCt8YcrTQ0PZKd11xwNuqCZ050qbgthQGtDTnd1inJzK9D4VcvjyNX0UjV3kY";
  static final String TOKEN_PREFIX = "Bearer ";
  static final String HEADER_STRING = "Authorization";

  static boolean validToken(String token) {
    try {
      Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);
      return true;
    } catch (Exception e) {
      return false;
    }
  }
}
