package br.com.f5global.base_dados_servicos.config;

import java.util.Arrays;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  public static final List<String> PATHS =
      Arrays.asList(
          "/v2/api-docs",
          "/swagger-resources",
          "/swagger-resources/configuration/ui",
          "/swagger-resources/configuration/security",
          "/swagger-ui.html",
          "/webjars/springfox-swagger-ui/favicon-32x32.png",
          "/webjars/springfox-swagger-ui/swagger-ui-bundle.js",
          "/webjars/springfox-swagger-ui/swagger-ui.css",
          "/webjars/springfox-swagger-ui/swagger-ui-standalone-preset.js",
          "/webjars/**/**");

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("br.com.f5global.base_dados_servicos.controller"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(metaData());
  }

  private ApiInfo metaData() {
    return new ApiInfoBuilder()
        .title("F5Global Bases dos Serviços")
        .description("Microserviço das funcionalidades de base de dados compartilhados nos serviços e grupos da F5Global")
        .version("1.0.0")
        .build();
  }
}
