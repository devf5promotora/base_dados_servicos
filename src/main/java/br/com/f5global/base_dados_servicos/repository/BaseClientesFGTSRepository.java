package br.com.f5global.base_dados_servicos.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.f5global.base_dados_servicos.model.BaseClientesFGTS;

@Repository
public interface BaseClientesFGTSRepository extends JpaRepository<BaseClientesFGTS, UUID>, JpaSpecificationExecutor<BaseClientesFGTS>{
	
	  @Query(
		      "SELECT p FROM BaseClientesFGTS p WHERE p.nomeCliente = :nome AND p.telefone = :telefone")
		  List<BaseClientesFGTS> findByNomeAndTelefone(
		      String nome,
		      String telefone);
	  
}
