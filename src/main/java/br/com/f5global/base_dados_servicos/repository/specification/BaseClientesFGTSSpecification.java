package br.com.f5global.base_dados_servicos.repository.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.f5global.base_dados_servicos.exception.BadRequestException;
import br.com.f5global.base_dados_servicos.model.BaseClientesFGTS;
import br.com.f5global.base_dados_servicos.model.BaseClientesFGTS_;
import br.com.f5global.base_dados_servicos.service.filter.BaseClientesFGTSFilter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BaseClientesFGTSSpecification implements Specification<BaseClientesFGTS> {

    /** */
    private static final long serialVersionUID = 6970841708640167374L;

    private final BaseClientesFGTSFilter filter;

    public static BaseClientesFGTSSpecification of(BaseClientesFGTSFilter filter) {
        if (filter == null) {
            return null;
        }
        return new BaseClientesFGTSSpecification(filter);
    }

    @Override
    public Predicate toPredicate(Root<BaseClientesFGTS> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        if (filter.getIdBaseClientesFgts() != null) {
            predicates.add(root.get(BaseClientesFGTS_.idBaseClientesFgts).in(filter.getIdBaseClientesFgts()));
        }
        if (filter.getTelefone() != null) {
            predicates.add(root.get(BaseClientesFGTS_.telefone).in(filter.getTelefone()));
        }
        if (filter.getNomeCliente() != null) {
			predicates.add(criteriaBuilder.like(root.get(BaseClientesFGTS_.nomeCliente), "%" + filter.getNomeCliente() + "%"));
        }
        
		if (filter.getDataCriacaoInicio() != null && filter.getDataCriacaoFim() == null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(BaseClientesFGTS_.dataCriacao),
					filter.getDataCriacaoInicio()));
		}
		if (filter.getDataCriacaoInicio() != null && filter.getDataCriacaoFim() != null) {
			if (filter.getDataCriacaoFim().isBefore(filter.getDataCriacaoInicio())) {
				throw new BadRequestException("Data final menor que a data inicio");
			}
			// condicao para buscar todas as comissoes taxas vigentes dentro de um intervalo
			predicates.add( criteriaBuilder.and(
					criteriaBuilder.lessThanOrEqualTo(root.get(BaseClientesFGTS_.dataCriacao), filter.getDataCriacaoFim()) ,
					criteriaBuilder.greaterThanOrEqualTo(root.get(BaseClientesFGTS_.dataCriacao), filter.getDataCriacaoInicio())));
		}
		
		if (filter.getDataManutencaoInicio() != null && filter.getDataManutencaoFim() == null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(BaseClientesFGTS_.dataCriacao),
					filter.getDataManutencaoInicio()));
		}
		if (filter.getDataManutencaoInicio() != null && filter.getDataManutencaoFim() != null) {
			if (filter.getDataManutencaoFim().isBefore(filter.getDataManutencaoInicio())) {
				throw new BadRequestException("Data final menor que a data inicio");
			}
			// condicao para buscar todas as comissoes taxas vigentes dentro de um intervalo
			predicates.add( criteriaBuilder.and(
					criteriaBuilder.lessThanOrEqualTo(root.get(BaseClientesFGTS_.dataCriacao), filter.getDataManutencaoFim()) ,
					criteriaBuilder.greaterThanOrEqualTo(root.get(BaseClientesFGTS_.dataCriacao), filter.getDataManutencaoInicio())));
		}

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

}