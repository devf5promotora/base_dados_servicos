package br.com.f5global.base_dados_servicos.repository.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.f5global.base_dados_servicos.exception.BadRequestException;
import br.com.f5global.base_dados_servicos.model.PreCadastroMarketPlace;
import br.com.f5global.base_dados_servicos.model.PreCadastroMarketPlace_;
import br.com.f5global.base_dados_servicos.service.filter.PreCadastroMarketPlaceFilter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PreCadastroMarketPlaceSpecification implements Specification<PreCadastroMarketPlace> {

    /** */
    private static final long serialVersionUID = 6970841708640167374L;

    private final PreCadastroMarketPlaceFilter filter;

    public static PreCadastroMarketPlaceSpecification of(PreCadastroMarketPlaceFilter filter) {
        if (filter == null) {
            return null;
        }
        return new PreCadastroMarketPlaceSpecification(filter);
    }

    @Override
    public Predicate toPredicate(Root<PreCadastroMarketPlace> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        if (filter.getIdPreCadastroMarketplace() != null) {
            predicates.add(root.get(PreCadastroMarketPlace_.idPreCadastroMarketplace).in(filter.getIdPreCadastroMarketplace()));
        }
        if (filter.getTelefone() != null) {
            predicates.add(root.get(PreCadastroMarketPlace_.telefone).in(filter.getTelefone()));
        }
        if (filter.getNomeParceiroMarketplace() != null) {
			predicates.add(criteriaBuilder.like(root.get(PreCadastroMarketPlace_.nomeParceiroMarketplace), "%" + filter.getNomeParceiroMarketplace() + "%"));
        }
        
		if (filter.getDataCriacaoInicio() != null && filter.getDataCriacaoFim() == null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(PreCadastroMarketPlace_.dataCriacao),
					filter.getDataCriacaoInicio()));
		}
		if (filter.getDataCriacaoInicio() != null && filter.getDataCriacaoFim() != null) {
			if (filter.getDataCriacaoFim().isBefore(filter.getDataCriacaoInicio())) {
				throw new BadRequestException("Data final menor que a data inicio");
			}
			// condicao para buscar todas as comissoes taxas vigentes dentro de um intervalo
			predicates.add( criteriaBuilder.and(
					criteriaBuilder.lessThanOrEqualTo(root.get(PreCadastroMarketPlace_.dataCriacao), filter.getDataCriacaoFim()) ,
					criteriaBuilder.greaterThanOrEqualTo(root.get(PreCadastroMarketPlace_.dataCriacao), filter.getDataCriacaoInicio())));
		}
		
		if (filter.getDataManutencaoInicio() != null && filter.getDataManutencaoFim() == null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(PreCadastroMarketPlace_.dataCriacao),
					filter.getDataManutencaoInicio()));
		}
		if (filter.getDataManutencaoInicio() != null && filter.getDataManutencaoFim() != null) {
			if (filter.getDataManutencaoFim().isBefore(filter.getDataManutencaoInicio())) {
				throw new BadRequestException("Data final menor que a data inicio");
			}
			// condicao para buscar todas as comissoes taxas vigentes dentro de um intervalo
			predicates.add( criteriaBuilder.and(
					criteriaBuilder.lessThanOrEqualTo(root.get(PreCadastroMarketPlace_.dataCriacao), filter.getDataManutencaoFim()) ,
					criteriaBuilder.greaterThanOrEqualTo(root.get(PreCadastroMarketPlace_.dataCriacao), filter.getDataManutencaoInicio())));
		}

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

}