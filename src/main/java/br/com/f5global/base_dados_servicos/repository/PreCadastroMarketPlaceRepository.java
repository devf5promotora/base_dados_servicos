package br.com.f5global.base_dados_servicos.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.f5global.base_dados_servicos.model.PreCadastroMarketPlace;

@Repository
public interface PreCadastroMarketPlaceRepository extends JpaRepository<PreCadastroMarketPlace, UUID>,JpaSpecificationExecutor<PreCadastroMarketPlace>  {

	  @Query(
		      "SELECT p FROM PreCadastroMarketPlace p WHERE p.nomeParceiroMarketplace = :nome AND p.telefone = :telefone")
		  List<PreCadastroMarketPlace> findByNomeAndTelefone(
		      String nome,
		      String telefone);
}
