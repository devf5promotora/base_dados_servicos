package br.com.f5global.base_dados_servicos.model.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import br.com.f5global.base_dados_servicos.model.constraints.validator.TelefoneValidator;

@Documented
@Constraint(validatedBy = TelefoneValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TelefoneConstraint {
    String message() default "Telefone Inválido! O telefone deve conter de 10 a 11 dígitos";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
