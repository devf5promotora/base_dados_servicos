package br.com.f5global.base_dados_servicos.model.constraints.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.f5global.base_dados_servicos.model.constraints.TelefoneConstraint;

public class TelefoneValidator implements ConstraintValidator<TelefoneConstraint, String> {

    @Override
    public void initialize(TelefoneConstraint telefone) {
    }

    @Override
    public boolean isValid(String telefone, ConstraintValidatorContext context) {
        //permitir que seja valido caso seja nulo ou vazio para nao travar salvar campo nao obrigatorio
        return telefone != null && !telefone.isBlank() ? telefone.length() >=10 && telefone.length() <=11 : false;
    }
    
}
