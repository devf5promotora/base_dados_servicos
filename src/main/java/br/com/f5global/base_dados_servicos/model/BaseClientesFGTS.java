package br.com.f5global.base_dados_servicos.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import br.com.f5global.base_dados_servicos.model.constraints.TelefoneConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
@Table(name = "base_clientes_fgts")
@Audited
@AuditTable(value = "base_clientes_fgts_audit")
public class BaseClientesFGTS {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(columnDefinition = "BINARY(16)", name = "id_base_clientes_fgts", unique = true, nullable = false)
	private UUID idBaseClientesFgts;
	
	@Column(name = "nome_cliente", nullable = false)
	private String nomeCliente;
	@Column(name = "telefone", nullable = false)
	@TelefoneConstraint
	private String telefone;
	@Column(name = "email", nullable = true)
	private String email;
	@Column(name = "ja_foi_encaminhado")
	private Boolean jaExtraido;
	@Column(name = "data_criacao")
	private LocalDate dataCriacao;
	@Column(name = "hora_criacao")
	private LocalTime horaCriacao;
	@Column(name = "usuario_criacao")
	private String usuarioCriacao;
	@Column(name = "data_manutencao")
	private LocalDate dataManutencao;
	@Column(name = "hora_manutencao")
	private LocalTime horaManutencao;
	@Column(name = "usuario_manutencao")
	private String usuarioManutencao;
	
	@PrePersist
	void criar() {
		LocalDateTime agora = LocalDateTime.now();
		dataCriacao = agora.toLocalDate();
		horaCriacao = agora.toLocalTime();
		jaExtraido = false;
	}
	
	@PreUpdate
	void atualizar() {
		LocalDateTime agora = LocalDateTime.now();
		dataManutencao = agora.toLocalDate();
		horaManutencao = agora.toLocalTime();
	}
}
