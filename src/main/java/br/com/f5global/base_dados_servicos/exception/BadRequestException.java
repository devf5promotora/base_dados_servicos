package br.com.f5global.base_dados_servicos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

  /** */
  private static final long serialVersionUID = 3070630663755808763L;

  public BadRequestException(String ex) {
    super(ex);
  }
}
