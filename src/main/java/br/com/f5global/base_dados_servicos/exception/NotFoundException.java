package br.com.f5global.base_dados_servicos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

  /** */
  private static final long serialVersionUID = -3880006193279958578L;

  public NotFoundException(String ex) {
    super(ex);
  }
}
