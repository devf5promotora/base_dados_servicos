package br.com.f5global.base_dados_servicos.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExceptionResponse {

  private String message;
  private String details;
}
