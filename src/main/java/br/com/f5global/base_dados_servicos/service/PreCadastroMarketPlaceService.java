package br.com.f5global.base_dados_servicos.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;

import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceCsvDTO;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.dto.RetornoListaPreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.filter.PreCadastroMarketPlaceFilter;
import br.com.f5global.base_dados_servicos.service.form.PreCadastroMarketPlaceForm;

public interface PreCadastroMarketPlaceService {

	//Criar base
	PreCadastroMarketPlaceDTO salvarPreCadastroMarketPlace(PreCadastroMarketPlaceForm form);
	//Deletar base
	void deletarPreCadastroMarketPlace(UUID uuidPreCadastroMarketPlace);
	//Atualizar base
	PreCadastroMarketPlaceDTO atualizarPreCadastroMarketPlace(UUID uuidPreCadastroMarketPlace,PreCadastroMarketPlaceForm form);
	//Listar Base
	RetornoListaPreCadastroMarketPlaceDTO listarPreCadastroMarketPlace(PreCadastroMarketPlaceFilter filter, Pageable page);
	//Gerar Arquivo Base
	List<PreCadastroMarketPlaceCsvDTO> gerarCsvPreCadastroMarketPlace(PreCadastroMarketPlaceFilter filter);
	
	void buscarBase();
}
