package br.com.f5global.base_dados_servicos.service.impl;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class ManipularCsv {

	public static boolean verificarCabecalho(String[] header, Resource resource, Charset charset, int headerPos) throws IOException {
		String[] cabecalhoArquivo = lerCabecalho(resource.getInputStream(), charset, headerPos);
		for (String nomeColuna : header) {
			if (Arrays.stream(cabecalhoArquivo).noneMatch(nomeColuna::equalsIgnoreCase))
				return false;
		}
		return true;
	}

	private static String[] lerCabecalho(InputStream stream, Charset charset, int headerPos) {
		try {
			String[] header = new String[0];
			CSVReader csvReader = new CSVReader(new InputStreamReader(stream, charset));
			for (int pos = 0; pos <= headerPos; pos++) {
				if (pos == headerPos) {
					header = csvReader.readNext();
				} else {
					csvReader.readNext();
				}
			}
			return header[0].split(";");
		} catch (Exception exception) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro ao ler o arquivo. Motivo: " + exception.getCause().getMessage());
		}
	}

	public static <T> List<T> ler_UTF_8(Class<T> clazz, Resource resource) throws IOException {
		try {
			return ler(clazz, resource.getInputStream(), Charset.forName("UTF-8"));
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro ao lê arquivo. " + e.getCause().getCause());
		}
	}

	public static <T> List<T> ler_ISO_8859_1(Class<T> clazz, Resource resource) throws IOException {
		try {
			return ler(clazz, resource.getInputStream(), Charset.forName("ISO-8859-1"));
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro ao lê arquivo. " + e.getCause().getCause());
		}
	}

	private static <T> List<T> ler(Class<T> clazz, InputStream input, Charset cs) {
		HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<T>();
		strategy.setType(clazz);

		CsvToBeanBuilder<T> csvBuilder = new CsvToBeanBuilder<>(new InputStreamReader(input, cs));

		return csvBuilder
				.withMappingStrategy(strategy)
				.withType(clazz)
				.withSeparator(';')
				.withIgnoreLeadingWhiteSpace(true)
				.build()
				.parse();
	}

	public static <T> List<T> ler_UTF_8(Class<T> clazz, MultipartFile file) throws IOException {
		try {
			if (!(FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("csv"))) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é permitido o envio de CSV.");
			}
			InputStream dado = file.getInputStream();

			return ler(IOUtils.toByteArray(dado), clazz, Charset.forName("UTF-8"));
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro ao lê arquivo. " + e.getCause().getCause());
		}
	}

	public static <T> List<T> ler_ISO_8859_1(Class<T> clazz, MultipartFile file) throws IOException {
		try {
			if (!(FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("csv"))) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é permitido o envio de CSV.");
			}

			InputStream dado = file.getInputStream();
			byte[] bytes = IOUtils.toByteArray(dado);

			return ler(bytes, clazz, Charset.forName("ISO-8859-1"));
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro ao lê arquivo. " + e.getMessage());
		}
	}

	public static <T> List<T> ler_ISO_8859_1Safra(Class<T> clazz, MultipartFile file)
			throws IOException {
		try {
			if (!(FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("csv"))) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é permitido o envio de CSV.");
			}

			InputStream dado = file.getInputStream();
			byte[] bytes = IOUtils.toByteArray(dado);

			return lerSafra(bytes, clazz, Charset.forName("ISO-8859-1"));
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro ao lê arquivo. " + e.getCause().getCause());
		}
	}

	private static <T> List<T> ler(byte[] bytes, Class<T> clazz, Charset cs) {
		HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<T>();
		strategy.setType(clazz);

		CsvToBeanBuilder<T> csvBuilder =
				new CsvToBeanBuilder<>(new InputStreamReader(new ByteArrayInputStream(bytes), cs));

		return csvBuilder
				.withMappingStrategy(strategy)
				.withType(clazz)
				.withSeparator(';')
				.withIgnoreLeadingWhiteSpace(true)
				.build()
				.parse();
	}

	private static <T> List<T> lerSafra(byte[] bytes, Class<T> clazz, Charset cs) {
		HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<T>();
		strategy.setType(clazz);

		CsvToBeanBuilder<T> csvBuilder =
				new CsvToBeanBuilder<>(new InputStreamReader(new ByteArrayInputStream(bytes), cs));

		return csvBuilder
				.withMappingStrategy(strategy)
				.withType(clazz)
				.withSeparator('§')
				.withIgnoreLeadingWhiteSpace(true)
				.build()
				.parse();
	}

	public static <Bean> Resource escrever_ISO_8859_1(Class<Bean> clazz, List<Bean> data)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		return escrever(clazz, data, Charset.forName("ISO-8859-1"));
	}

	public static <Bean> Resource escrever_ISO_8859_2(Class<Bean> clazz, List<Bean> data)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		return escrever2(clazz, data, Charset.forName("ISO-8859-1"));
	}

	public static <Bean> Resource escrever2(Class<Bean> clazz, List<Bean> data, Charset cs)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		if (data.isEmpty()) {
			return null;
		}
		for (Bean d : data) {
			ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
			BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, cs));
			CustomMappingStrategy<Bean> strategy = new CustomMappingStrategy<Bean>();

			strategy.setType(clazz);

			StatefulBeanToCsv<Bean> sbcData =
					new StatefulBeanToCsvBuilder<Bean>(inMemoryStream)
					.withMappingStrategy(strategy)
					.withSeparator(';')
					.build();
			try {
				sbcData.write(d);
			} catch (CsvDataTypeMismatchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CsvRequiredFieldEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Resource res = new ByteArrayResource(inMemory.toByteArray());
		}

		ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
		BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, cs));

		CustomMappingStrategy<Bean> strategy = new CustomMappingStrategy<Bean>();
		strategy.setType(clazz);

		StatefulBeanToCsv<Bean> sbcData =
				new StatefulBeanToCsvBuilder<Bean>(inMemoryStream)
				.withMappingStrategy(strategy)
				.withSeparator(';')
				.build();
		sbcData.write(data);
		inMemoryStream.flush();

		inMemoryStream.close();

		return new ByteArrayResource(inMemory.toByteArray());
	}

	public static <Bean> Resource escrever_UTF_8(Class<Bean> clazz, List<Bean> data)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		return escrever(clazz, data, Charset.forName("UTF-8"));
	}

	public static <Bean> Resource escrever(Class<Bean> clazz, List<Bean> data, Charset cs)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		if (data.isEmpty()) {
			return null;
		}
		for (Bean d : data) {
			ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
			BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, cs));
			HeaderColumnNameMappingStrategy<Bean> strategy = new HeaderColumnNameMappingStrategy<Bean>();

			strategy.setType(clazz);

			StatefulBeanToCsv<Bean> sbcData =
					new StatefulBeanToCsvBuilder<Bean>(inMemoryStream)
					.withMappingStrategy(strategy)
					.withSeparator(';')
					.build();
			try {
				sbcData.write(d);
			} catch (CsvDataTypeMismatchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CsvRequiredFieldEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Resource res = new ByteArrayResource(inMemory.toByteArray());
		}

		ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
		BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, cs));

		HeaderColumnNameMappingStrategy<Bean> strategy = new HeaderColumnNameMappingStrategy<Bean>();
		strategy.setType(clazz);

		StatefulBeanToCsv<Bean> sbcData =
				new StatefulBeanToCsvBuilder<Bean>(inMemoryStream)
				.withMappingStrategy(strategy)
				.withSeparator(';')
				.build();
		sbcData.write(data);
		inMemoryStream.flush();

		inMemoryStream.close();

		return new ByteArrayResource(inMemory.toByteArray());
	}

	public <Bean> Resource escrever_ISO_8859_1Posicao(Class<Bean> clazz, List<Bean> data)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		return escreverPosicaoCsv(clazz, data, Charset.forName("ISO-8859-1"));
	}

	public static <Bean> Resource escrever_UFT_Posicao(Class<Bean> clazz, List<Bean> data)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		return escreverPosicaoCsv(clazz, data, Charset.forName("UTF-8"));
	}

	public static <Bean> Resource escreverPosicaoCsv(Class<Bean> clazz, List<Bean> data, Charset cs)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		if (data.isEmpty()) {
			return null;
		}
		for (Bean d : data) {
			ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
			BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, cs));

			CustomMappingStrategy<Bean> strategy = new CustomMappingStrategy<Bean>();
			strategy.setType(clazz);

			StatefulBeanToCsv<Bean> writer =
					new StatefulBeanToCsvBuilder<Bean>(inMemoryStream)
					.withMappingStrategy(strategy)
					.withSeparator(';')
					.withApplyQuotesToAll(false)
					.build();

			try {
				writer.write(d);

			} catch (CsvDataTypeMismatchException e) {
				e.printStackTrace();
			} catch (CsvRequiredFieldEmptyException e) {
				e.printStackTrace();
			}

			Resource res = new ByteArrayResource(inMemory.toByteArray());
		}

		ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
		BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, cs));

		CustomMappingStrategy<Bean> strategy = new CustomMappingStrategy<Bean>();
		strategy.setType(clazz);

		StatefulBeanToCsv<Bean> writer =
				new StatefulBeanToCsvBuilder<Bean>(inMemoryStream)
				.withMappingStrategy(strategy)
				.withSeparator(';')
				.withApplyQuotesToAll(false)
				.build();
		writer.write(data);
		try {
			inMemoryStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new ByteArrayResource(inMemory.toByteArray());
	}
}

