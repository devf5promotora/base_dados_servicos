package br.com.f5global.base_dados_servicos.service.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class PreCadastroMarketPlaceDTO {

	private UUID idPreCadastroMarketplace;
	private String nomeParceiroMarketplace;
	private String telefone;
	private String email;
	private Boolean parceiroAceitaFornecerDados;
	private Boolean jaExtraido;
	private LocalDate dataCriacao;
	private LocalTime horaCriacao;
	private String usuarioCriacao;
	private LocalDate dataManutencao;
	private LocalTime horaManutencao;
	private String usuarioManutencao;
}
