package br.com.f5global.base_dados_servicos.service.impl;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.BadRequestException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import br.com.f5global.base_dados_servicos.client.AutenticacaoClient;
import br.com.f5global.base_dados_servicos.client.GerenciadorArquivoClient;
import br.com.f5global.base_dados_servicos.client.ServicoImportadorClient;
import br.com.f5global.base_dados_servicos.client.dto.Authorization;
import br.com.f5global.base_dados_servicos.client.dto.DadosLoginTelugo;
import br.com.f5global.base_dados_servicos.client.dto.GerenciadorArquivoDTO;
import br.com.f5global.base_dados_servicos.client.dto.RetornoUploadDTO;
import br.com.f5global.base_dados_servicos.client.enums.NomeServico;
import br.com.f5global.base_dados_servicos.client.enums.TipoArquivo;
import br.com.f5global.base_dados_servicos.exception.NotFoundException;
import br.com.f5global.base_dados_servicos.model.PreCadastroMarketPlace;
import br.com.f5global.base_dados_servicos.repository.PreCadastroMarketPlaceRepository;
import br.com.f5global.base_dados_servicos.repository.specification.PreCadastroMarketPlaceSpecification;
import br.com.f5global.base_dados_servicos.service.PreCadastroMarketPlaceService;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceCsvDTO;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.dto.RetornoListaPreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.filter.PreCadastroMarketPlaceFilter;
import br.com.f5global.base_dados_servicos.service.form.PreCadastroMarketPlaceForm;
import br.com.f5global.base_dados_servicos.service.mapper.PreCadastroMarketPlaceMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
@EnableScheduling
public class PreCadastroMarketPlaceServiceImpl implements PreCadastroMarketPlaceService {

	@Value("${username}")
	private String username;
	@Value("${password}")
	private String password;
	@Value("${emaildownloadmarketplace}")
	private String emaildownload;
	@Value("${emaildownloadmarketplace2}")
	private String emaildownload2;

	private final PreCadastroMarketPlaceMapper mapper;
	private final PreCadastroMarketPlaceRepository repo;
	private final GerenciadorArquivoClient gerenciadorClient;
	private final ServicoImportadorClient importadorClient;
	private final AutenticacaoClient autenticacaoClient;
	private final VerificarEmailService emailService;

	@Override
	public PreCadastroMarketPlaceDTO salvarPreCadastroMarketPlace(PreCadastroMarketPlaceForm form) {
		try {
			PreCadastroMarketPlace PreCadastroMarketPlaceSalvar = mapper.toEntity(form);
			if(form.getEmail() != null && !form.getEmail().isEmpty()  && !emailService.validandoEmail(form.getEmail())){
				throw new BadRequestException("Email Inválido!");
			}
			List<PreCadastroMarketPlace> listaCliente = repo.findByNomeAndTelefone(form.getNomeParceiroMarketplace(), form.getTelefone());
			if (listaCliente != null && !listaCliente.isEmpty()) {
				log.info("Parceiro já esta na base");
				return null;
			}
			log.info("Salvando o parceiro");
			try {
				PreCadastroMarketPlaceSalvar = repo.saveAndFlush(PreCadastroMarketPlaceSalvar);
			} catch (Exception e) {
				throw new BadRequestException(e.getCause().getCause());
			}
			return mapper.toDto(PreCadastroMarketPlaceSalvar);
		} catch (Exception e) {
			throw new BadRequestException("Não foi possível concluir ação, motivo: " + e.getMessage());
		}
	}

	@Override
	public void deletarPreCadastroMarketPlace(UUID uuidPreCadastroMarketPlace) {
		try {
			Optional<PreCadastroMarketPlace> PreCadastroMarketPlaceSalvar = repo.findById(uuidPreCadastroMarketPlace);
			if (PreCadastroMarketPlaceSalvar.isPresent()) {
				log.info("Deletando o parceiro: " + PreCadastroMarketPlaceSalvar.get().getNomeParceiroMarketplace());
				repo.delete(PreCadastroMarketPlaceSalvar.get());
				return ;
			} else {
				throw new BadRequestException("Não foi localizado na base este parceiro.");
			}
		} catch (Exception e) {
			throw new BadRequestException("Não foi possível concluir ação, motivo: " + e.getMessage());
		}
	}

	@Override
	public PreCadastroMarketPlaceDTO atualizarPreCadastroMarketPlace(UUID uuidPreCadastroMarketPlace, PreCadastroMarketPlaceForm form) {
		try {
			Optional<PreCadastroMarketPlace> PreCadastroMarketPlaceSalvar = repo.findById(uuidPreCadastroMarketPlace);
			if (PreCadastroMarketPlaceSalvar.isPresent()) {
				if(form.getEmail() != null && !form.getEmail().isEmpty()  && !emailService.validandoEmail(form.getEmail())){
					throw new BadRequestException("Email Inválido!");
				}
				PreCadastroMarketPlace PreCadastroMarketPlaceAtualizar = mapper.atualizar(form, PreCadastroMarketPlaceSalvar.get());
				log.info("Alterando o parceiro: " + PreCadastroMarketPlaceSalvar.get().getNomeParceiroMarketplace());
				PreCadastroMarketPlaceAtualizar = repo.saveAndFlush(PreCadastroMarketPlaceAtualizar);
				return mapper.toDto(PreCadastroMarketPlaceAtualizar);
			} else {
				throw new BadRequestException("Não foi localizado na base este parceiro.");
			}
		} catch (Exception e) {
			throw new BadRequestException("Não foi possível concluir atualizar o parceiro, motivo: " + e.getMessage());
		}
	}

	@Override
	public RetornoListaPreCadastroMarketPlaceDTO listarPreCadastroMarketPlace(PreCadastroMarketPlaceFilter filter, Pageable page) {
		Page<PreCadastroMarketPlaceDTO> listaPreCadastroMarketPlace = repo.findAll(PreCadastroMarketPlaceSpecification.of(filter),page).map(mapper::toDto);
		log.info("Buscando lista de parceiros.");
		if (listaPreCadastroMarketPlace.hasContent()) {
			return RetornoListaPreCadastroMarketPlaceDTO.builder()
					.setTotalPaginas(listaPreCadastroMarketPlace.getTotalPages())
					.setTotalElementos(listaPreCadastroMarketPlace.getTotalElements())
					.setListaPreCadastroMarketPlace(listaPreCadastroMarketPlace.getContent())
					.build();
		} else {
			throw new NotFoundException("Nenhum parceiro encontrado.");
		}
	}

	@Override
	public List<PreCadastroMarketPlaceCsvDTO> gerarCsvPreCadastroMarketPlace(PreCadastroMarketPlaceFilter filter) {
		Page<PreCadastroMarketPlace> listaPreCadastroMarketPlace = repo.findAll(PreCadastroMarketPlaceSpecification.of(filter), PageRequest.of(0, Integer.MAX_VALUE));
		log.info("Buscando lista de parceiros.");
		if (listaPreCadastroMarketPlace.hasContent()) {
			//Mapear csv.
			return mapper.gerarCsv(listaPreCadastroMarketPlace.getContent());
		} else {
			throw new NotFoundException("Nenhum parceiro encontrado.");
		}
	}

	@Scheduled(cron = "0 0 7-20 * * *", zone = "America/Sao_Paulo")
	@Override
	public void buscarBase() {
		Page<PreCadastroMarketPlace> listaPreCadastroMarketPlace = repo.findAll(PreCadastroMarketPlaceSpecification.of(PreCadastroMarketPlaceFilter.builder()
				.setDataCriacaoFim(LocalDate.now())
				.setDataCriacaoInicio(LocalDate.now().minusDays(5))
				.build()), PageRequest.of(0, Integer.MAX_VALUE));
		Authorization token = new Authorization();
		try { 
			token = autenticacaoClient.login(DadosLoginTelugo.builder()
					.setUsername(username)
					.setPassword(password)
					.build());
		} catch (Exception e) {
			log.info("Falha ao obter token Telugo");
		}

		log.info("Buscando lista de parceiros.");
		if (listaPreCadastroMarketPlace.hasContent()) {
			//Mapear csv.
			List<PreCadastroMarketPlaceCsvDTO> listaCsv = mapper.gerarCsv(listaPreCadastroMarketPlace.getContent());
			this.salvarArquivo(PreCadastroMarketPlaceCsvDTO.class, listaCsv, token.getToken(), "PreCadastroMarketPlace.csv");
			listaPreCadastroMarketPlace.getContent().stream().filter(p->(p.getJaExtraido() == null || !p.getJaExtraido())).forEach(r->{
				r.setJaExtraido(true);
				repo.saveAndFlush(r);
			});
		} else {
			throw new NotFoundException("Nenhum parceiro encontrado.");
		}
		return ;
	}

	private <T> ByteArrayResource gerarCSV(Class<T> clazz, List<T> dadosArquivo) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		log.info("Gerando csv...");
		ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
		BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, StandardCharsets.UTF_8));
		CustomMappingStrategy<T> mappingStrategy = new CustomMappingStrategy<T>();
		mappingStrategy.setType(clazz);
		StatefulBeanToCsv<T> sbcData = new StatefulBeanToCsvBuilder<T>(inMemoryStream)
				.withMappingStrategy(mappingStrategy)
				.withSeparator(';')
				.build();
		sbcData.write(dadosArquivo);
		inMemoryStream.flush();
		inMemoryStream.close();
		return new ByteArrayResource(inMemory.toByteArray());
	}

	private <T> void salvarArquivo(Class<T> clazz, List<T> retornoErro, String token, String nomeArquivo) {
		try {
			log.info("Enviando relatorio de erros da importação para o gerenciador...");
			String[] fileName = nomeArquivo.split("\\.");
			ByteArrayResource resource = gerarCSV(clazz, retornoErro);
			MultipartFile arquivo = new MockMultipartFile(fileName[0], fileName[0] + ".csv", "text/csv", resource.getInputStream());
			RetornoUploadDTO retornoUploadDTO = importadorClient.importarArquivo(arquivo, TipoArquivo.DOCUMENTO, token);
			GerenciadorArquivoDTO gerenciador = gerenciadorClient.cadastrar(token, NomeServico.MARKETPLACE);
			gerenciadorClient.atualizarLink(token, gerenciador.getUuidGerenciadorArquivo(), retornoUploadDTO.getDiretorioDoArquivo());
			gerenciadorClient.atualizarUsuario(token, gerenciador.getUuidGerenciadorArquivo(), emaildownload);
			//Novo E-mail
			if (!emaildownload.equals(emaildownload2)) {
				GerenciadorArquivoDTO gerenciador2 = gerenciadorClient.cadastrar(token, NomeServico.MARKETPLACE);
				gerenciadorClient.atualizarLink(token, gerenciador2.getUuidGerenciadorArquivo(), retornoUploadDTO.getDiretorioDoArquivo());
				gerenciadorClient.atualizarUsuario(token, gerenciador2.getUuidGerenciadorArquivo(), emaildownload2);
			}
		} catch (Exception e) {
		}
	}

}

