package br.com.f5global.base_dados_servicos.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;

import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSCsvDto;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.dto.RetornoListaBaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.filter.BaseClientesFGTSFilter;
import br.com.f5global.base_dados_servicos.service.form.BaseClientesFGTSForm;

public interface BaseClientesFGTSService {

	//Criar base
	BaseClientesFGTSDto salvarBaseClientesFGTS(BaseClientesFGTSForm form);
	//Deletar base
	void deletarBaseClientesFGTS(UUID uuidBaseClientesFGTS);
	//Atualizar base
	BaseClientesFGTSDto atualizarBaseClientesFGTS(UUID uuidBaseClientesFGTS,BaseClientesFGTSForm form);
	//Listar Base
	RetornoListaBaseClientesFGTSDto listarBaseClientesFGTS(BaseClientesFGTSFilter filter, Pageable page);
	//Gerar Arquivo Base
	List<BaseClientesFGTSCsvDto> gerarCsvBaseClientesFGTS(BaseClientesFGTSFilter filter);
	
	void buscarBase();
}
