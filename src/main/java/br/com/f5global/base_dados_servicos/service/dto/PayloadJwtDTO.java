package br.com.f5global.base_dados_servicos.service.dto;

import java.util.Base64;
import java.util.List;

import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class PayloadJwtDTO {

  private String iss;
  private String sub;
  private String empresaMaster;
  private String usuarioNome;
  private Long usuarioParceiro;
  private List<String> authorities;
  private String equipe;
  private Long iat;
  private Long exp;
    
  public static PayloadJwtDTO recuperarUsuarioPorToken(String token) {

	    Base64.Decoder decoder = Base64.getDecoder();
	    String[] chunks = token.split(" ");

	    String[] chunks2 = chunks[1].split("\\.");
	    String payload = new String(decoder.decode(chunks2[1]));

	    Gson gson = new Gson();

	    String payloadJson = gson.toJson(payload);
	    payloadJson = payload.replace("\\", "");
	    payloadJson = payload.replace("Bearer ", "");
	    PayloadJwtDTO payloadJwtDTO = gson.fromJson(payloadJson, PayloadJwtDTO.class);

	    return payloadJwtDTO;
	  }
}

