package br.com.f5global.base_dados_servicos.service.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class BaseClientesFGTSCsvDto {

	@CsvBindByName(column = "Nome do Cliente")
	@CsvBindByPosition(position = 0)
	private String nomeCliente;
	@CsvBindByName(column = "Telefone do cliente")
	@CsvBindByPosition(position = 1)
	private String telefone;
	@CsvBindByName(column = "E-mail do cliente")
	@CsvBindByPosition(position = 2)
	private String email;
	@CsvBindByName(column = "Data do Cadastro")
	@CsvBindByPosition(position = 3)
	private LocalDate dataCriacao;
	@CsvBindByName(column = "Hora do Cadastro")
	@CsvBindByPosition(position = 4)
	private LocalTime horaCriacao;
	@CsvBindByName(column = "Data da Modificação")
	@CsvBindByPosition(position = 5)
	private LocalDate dataManutencao;
	@CsvBindByName(column = "Hora da Modificação")
	@CsvBindByPosition(position = 6)
	private LocalTime horaManutencao;
	@CsvBindByName(column = "Observação")
	@CsvBindByPosition(position = 7)
	private String jaExtraidoAntes;
}
