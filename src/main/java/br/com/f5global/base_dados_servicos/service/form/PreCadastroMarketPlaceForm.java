package br.com.f5global.base_dados_servicos.service.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.f5global.base_dados_servicos.model.constraints.TelefoneConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class PreCadastroMarketPlaceForm {

	@NotNull(message = "O nome não pode ser nulo/vazio")
	@NotEmpty(message = "O nome não pode ser nulo/vazio")
	private String nomeParceiroMarketplace;
	@NotNull(message = "O parceiro precisa informar se aceita fornecer seus dados")
	private Boolean parceiroAceitaFornecerDados;
	@NotNull(message = "O telefone não pode ser nulo/vazio")
	@NotEmpty(message = "O telefone não pode ser nulo/vazio")
	@TelefoneConstraint
	private String telefone;
	//Opcional
	private String email;
}
