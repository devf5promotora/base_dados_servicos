package br.com.f5global.base_dados_servicos.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import br.com.f5global.base_dados_servicos.model.PreCadastroMarketPlace;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceCsvDTO;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.form.PreCadastroMarketPlaceForm;

@Mapper(
	    componentModel = "spring",
	    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PreCadastroMarketPlaceMapper {

	PreCadastroMarketPlace copiar(PreCadastroMarketPlace entity);
	PreCadastroMarketPlace atualizar(PreCadastroMarketPlaceForm form, @MappingTarget PreCadastroMarketPlace entity);
	
	@Mapping(target = "jaExtraidoAntes", expression = "java(dto.getJaExtraido() != null && dto.getJaExtraido() ? \"Já foi enviado em bases anteriores\" : null)")
	PreCadastroMarketPlaceCsvDTO gerarCsv(PreCadastroMarketPlace dto);
	List<PreCadastroMarketPlaceCsvDTO> gerarCsv(List<PreCadastroMarketPlace> dtos);
	
	PreCadastroMarketPlace toEntity(PreCadastroMarketPlaceForm form);
	PreCadastroMarketPlaceDTO toDto(PreCadastroMarketPlaceForm form);
	PreCadastroMarketPlaceDTO toDto(PreCadastroMarketPlace entity);
	PreCadastroMarketPlace toEntity(PreCadastroMarketPlaceDTO dto);
	
	List<PreCadastroMarketPlace> toEntityForm(List<PreCadastroMarketPlaceForm> form);
	List<PreCadastroMarketPlaceDTO> toDtoForm(List<PreCadastroMarketPlaceForm> form);
	List<PreCadastroMarketPlaceDTO> toDto(List<PreCadastroMarketPlace> entity);
	List<PreCadastroMarketPlace> toEntity(List<PreCadastroMarketPlaceDTO> dto);
}
