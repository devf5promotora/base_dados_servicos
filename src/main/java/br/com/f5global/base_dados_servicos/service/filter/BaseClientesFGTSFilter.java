package br.com.f5global.base_dados_servicos.service.filter;

import java.time.LocalDate;
import java.util.UUID;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class BaseClientesFGTSFilter {

	private UUID idBaseClientesFgts;
	private String nomeCliente;
	private String telefone;
	private Boolean jaExtraido;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataCriacaoInicio;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataCriacaoFim;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataManutencaoInicio;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataManutencaoFim;
}
