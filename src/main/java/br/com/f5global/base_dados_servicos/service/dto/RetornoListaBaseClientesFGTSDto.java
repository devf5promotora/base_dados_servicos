package br.com.f5global.base_dados_servicos.service.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class RetornoListaBaseClientesFGTSDto {

	private List<BaseClientesFGTSDto> listaBaseClientesFgts;
	private Integer totalPaginas;
	private Long totalElementos;

}
