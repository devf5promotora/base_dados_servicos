package br.com.f5global.base_dados_servicos.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import br.com.f5global.base_dados_servicos.model.BaseClientesFGTS;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSCsvDto;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.form.BaseClientesFGTSForm;

@Mapper(
	    componentModel = "spring",
	    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface BaseClientesFGTSMapper {
	
	BaseClientesFGTS copiar(BaseClientesFGTS entity);
	BaseClientesFGTS atualizar(BaseClientesFGTSForm form, @MappingTarget BaseClientesFGTS entity);
	
	@Mapping(target = "jaExtraidoAntes", expression = "java(dto.getJaExtraido() != null && dto.getJaExtraido() ? \"Já foi enviado em bases anteriores\" : null)")
	BaseClientesFGTSCsvDto gerarCsv(BaseClientesFGTS dto);
	List<BaseClientesFGTSCsvDto> gerarCsv(List<BaseClientesFGTS> dtos);
	
	BaseClientesFGTS toEntity(BaseClientesFGTSForm form);
	BaseClientesFGTSDto toDto(BaseClientesFGTSForm form);
	BaseClientesFGTSDto toDto(BaseClientesFGTS entity);
	BaseClientesFGTS toEntity(BaseClientesFGTSDto dto);
	
	List<BaseClientesFGTS> toEntityForm(List<BaseClientesFGTSForm> form);
	List<BaseClientesFGTSDto> toDtoForm(List<BaseClientesFGTSForm> form);
	List<BaseClientesFGTSDto> toDto(List<BaseClientesFGTS> entity);
	List<BaseClientesFGTS> toEntity(List<BaseClientesFGTSDto> dto);
}
