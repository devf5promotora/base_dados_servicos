package br.com.f5global.base_dados_servicos.service.impl;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.BadRequestException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import br.com.f5global.base_dados_servicos.client.AutenticacaoClient;
import br.com.f5global.base_dados_servicos.client.GerenciadorArquivoClient;
import br.com.f5global.base_dados_servicos.client.ServicoImportadorClient;
import br.com.f5global.base_dados_servicos.client.dto.Authorization;
import br.com.f5global.base_dados_servicos.client.dto.DadosLoginTelugo;
import br.com.f5global.base_dados_servicos.client.dto.GerenciadorArquivoDTO;
import br.com.f5global.base_dados_servicos.client.dto.RetornoUploadDTO;
import br.com.f5global.base_dados_servicos.client.enums.NomeServico;
import br.com.f5global.base_dados_servicos.client.enums.TipoArquivo;
import br.com.f5global.base_dados_servicos.exception.NotFoundException;
import br.com.f5global.base_dados_servicos.model.BaseClientesFGTS;
import br.com.f5global.base_dados_servicos.repository.BaseClientesFGTSRepository;
import br.com.f5global.base_dados_servicos.repository.specification.BaseClientesFGTSSpecification;
import br.com.f5global.base_dados_servicos.service.BaseClientesFGTSService;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSCsvDto;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.dto.RetornoListaBaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.filter.BaseClientesFGTSFilter;
import br.com.f5global.base_dados_servicos.service.form.BaseClientesFGTSForm;
import br.com.f5global.base_dados_servicos.service.mapper.BaseClientesFGTSMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
@EnableScheduling
public class BaseClientesFGTSServiceImpl implements BaseClientesFGTSService {

	@Value("${username}")
	private String username;
	@Value("${password}")
	private String password;
	@Value("${emaildownload}")
	private String emaildownload;

	private final BaseClientesFGTSMapper mapper;
	private final BaseClientesFGTSRepository repo;
	private final GerenciadorArquivoClient gerenciadorClient;
	private final ServicoImportadorClient importadorClient;
	private final AutenticacaoClient autenticacaoClient;
	private final VerificarEmailService emailService;

	@Override
	public BaseClientesFGTSDto salvarBaseClientesFGTS(BaseClientesFGTSForm form) {
		try {
			BaseClientesFGTS baseClientesFgtsSalvar = mapper.toEntity(form);
			if(form.getEmail() != null && !form.getEmail().isEmpty()  && !emailService.validandoEmail(form.getEmail())){
				throw new BadRequestException("Email Inválido!");
			}
			List<BaseClientesFGTS> listaCliente = repo.findByNomeAndTelefone(form.getNomeCliente(), form.getTelefone());
			if (listaCliente != null && !listaCliente.isEmpty()) {
				log.info("Cliente já esta na base");
				return null;
			}
			log.info("Salvando o cliente");
			try {
				baseClientesFgtsSalvar = repo.saveAndFlush(baseClientesFgtsSalvar);
			} catch (Exception e) {
				throw new BadRequestException(e.getCause().getCause());
			}
			return mapper.toDto(baseClientesFgtsSalvar);
		} catch (Exception e) {
			throw new BadRequestException("Não foi possível concluir ação, motivo: " + e.getMessage());
		}
	}

	@Override
	public void deletarBaseClientesFGTS(UUID uuidBaseClientesFGTS) {
		try {
			Optional<BaseClientesFGTS> baseClientesFgtsSalvar = repo.findById(uuidBaseClientesFGTS);
			if (baseClientesFgtsSalvar.isPresent()) {
				log.info("Deletando o cliente: " + baseClientesFgtsSalvar.get().getNomeCliente());
				repo.delete(baseClientesFgtsSalvar.get());
				return ;
			} else {
				throw new BadRequestException("Não foi localizado na base este cliente.");
			}
		} catch (Exception e) {
			throw new BadRequestException("Não foi possível concluir ação, motivo: " + e.getMessage());
		}
	}

	@Override
	public BaseClientesFGTSDto atualizarBaseClientesFGTS(UUID uuidBaseClientesFGTS, BaseClientesFGTSForm form) {
		try {
			Optional<BaseClientesFGTS> baseClientesFgtsSalvar = repo.findById(uuidBaseClientesFGTS);
			if (baseClientesFgtsSalvar.isPresent()) {
				if(form.getEmail() != null && !form.getEmail().isEmpty()  && !emailService.validandoEmail(form.getEmail())){
					throw new BadRequestException("Email Inválido!");
				}
				BaseClientesFGTS baseClientesFgtsAtualizar = mapper.atualizar(form, baseClientesFgtsSalvar.get());
				log.info("Alterando o cliente: " + baseClientesFgtsSalvar.get().getNomeCliente());
				baseClientesFgtsAtualizar = repo.saveAndFlush(baseClientesFgtsAtualizar);
				return mapper.toDto(baseClientesFgtsAtualizar);
			} else {
				throw new BadRequestException("Não foi localizado na base este cliente.");
			}
		} catch (Exception e) {
			throw new BadRequestException("Não foi possível concluir atualizar o cliente, motivo: " + e.getMessage());
		}
	}

	@Override
	public RetornoListaBaseClientesFGTSDto listarBaseClientesFGTS(BaseClientesFGTSFilter filter, Pageable page) {
		Page<BaseClientesFGTSDto> listaBaseClientesFgts = repo.findAll(BaseClientesFGTSSpecification.of(filter),page).map(mapper::toDto);
		log.info("Buscando lista de clientes.");
		if (listaBaseClientesFgts.hasContent()) {
			return RetornoListaBaseClientesFGTSDto.builder()
					.setTotalPaginas(listaBaseClientesFgts.getTotalPages())
					.setTotalElementos(listaBaseClientesFgts.getTotalElements())
					.setListaBaseClientesFgts(listaBaseClientesFgts.getContent())
					.build();
		} else {
			throw new NotFoundException("Nenhum cliente encontrado.");
		}
	}

	@Override
	public List<BaseClientesFGTSCsvDto> gerarCsvBaseClientesFGTS(BaseClientesFGTSFilter filter) {
		Page<BaseClientesFGTS> listaBaseClientesFgts = repo.findAll(BaseClientesFGTSSpecification.of(filter), PageRequest.of(0, Integer.MAX_VALUE));
		log.info("Buscando lista de clientes.");
		if (listaBaseClientesFgts.hasContent()) {
			//Mapear csv.
			return mapper.gerarCsv(listaBaseClientesFgts.getContent());
		} else {
			throw new NotFoundException("Nenhum cliente encontrado.");
		}
	}

	@Scheduled(cron = "0 0 7-20 * * *", zone = "America/Sao_Paulo")
	@Override
	public void buscarBase() {
		Page<BaseClientesFGTS> listaBaseClientesFgts = repo.findAll(BaseClientesFGTSSpecification.of(BaseClientesFGTSFilter.builder()
				.setDataCriacaoFim(LocalDate.now())
				.setDataCriacaoInicio(LocalDate.now().minusDays(5))
				.build()), PageRequest.of(0, Integer.MAX_VALUE));
		Authorization token = new Authorization();
		try { 
			token = autenticacaoClient.login(DadosLoginTelugo.builder()
					.setUsername(username)
					.setPassword(password)
					.build());
		} catch (Exception e) {
			log.info("Falha ao obter token Telugo");
		}

		log.info("Buscando lista de clientes.");
		if (listaBaseClientesFgts.hasContent()) {
			//Mapear csv.
			List<BaseClientesFGTSCsvDto> listaCsv = mapper.gerarCsv(listaBaseClientesFgts.getContent());
			this.salvarArquivo(BaseClientesFGTSCsvDto.class, listaCsv, token.getToken(), "baseClientesFgts.csv");
			listaBaseClientesFgts.getContent().stream().filter(p->(p.getJaExtraido() == null || !p.getJaExtraido())).forEach(r->{
				r.setJaExtraido(true);
				repo.saveAndFlush(r);
			});
		} else {
			throw new NotFoundException("Nenhum cliente encontrado.");
		}
		return ;
	}

	private <T> ByteArrayResource gerarCSV(Class<T> clazz, List<T> dadosArquivo) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		log.info("Gerando csv...");
		ByteArrayOutputStream inMemory = new ByteArrayOutputStream();
		BufferedWriter inMemoryStream = new BufferedWriter(new OutputStreamWriter(inMemory, StandardCharsets.UTF_8));
		CustomMappingStrategy<T> mappingStrategy = new CustomMappingStrategy<T>();
		mappingStrategy.setType(clazz);
		StatefulBeanToCsv<T> sbcData = new StatefulBeanToCsvBuilder<T>(inMemoryStream)
				.withMappingStrategy(mappingStrategy)
				.withSeparator(';')
				.build();
		sbcData.write(dadosArquivo);
		inMemoryStream.flush();
		inMemoryStream.close();
		return new ByteArrayResource(inMemory.toByteArray());
	}


	private <T> void salvarArquivo(Class<T> clazz, List<T> retornoErro, String token, String nomeArquivo) {
		try {
			log.info("Enviando relatorio de erros da importação para o gerenciador...");
			String[] fileName = nomeArquivo.split("\\.");
			ByteArrayResource resource = gerarCSV(clazz, retornoErro);
			MultipartFile arquivo = new MockMultipartFile(fileName[0], fileName[0] + ".csv", "text/csv", resource.getInputStream());
			RetornoUploadDTO retornoUploadDTO = importadorClient.importarArquivo(arquivo, TipoArquivo.DOCUMENTO, token);
			GerenciadorArquivoDTO gerenciador = gerenciadorClient.cadastrar(token, NomeServico.BASE_FGTS);
			gerenciadorClient.atualizarLink(token, gerenciador.getUuidGerenciadorArquivo(), retornoUploadDTO.getDiretorioDoArquivo());
			gerenciadorClient.atualizarUsuario(token, gerenciador.getUuidGerenciadorArquivo(), emaildownload);
		} catch (Exception e) {
		}
	}

}
