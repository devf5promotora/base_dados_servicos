package br.com.f5global.base_dados_servicos.controller;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.f5global.base_dados_servicos.exception.BadRequestException;
import br.com.f5global.base_dados_servicos.service.BaseClientesFGTSService;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSCsvDto;
import br.com.f5global.base_dados_servicos.service.dto.BaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.dto.RetornoListaBaseClientesFGTSDto;
import br.com.f5global.base_dados_servicos.service.filter.BaseClientesFGTSFilter;
import br.com.f5global.base_dados_servicos.service.form.BaseClientesFGTSForm;
import br.com.f5global.base_dados_servicos.service.impl.ManipularCsv;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/base-clientes-fgts")
@RequiredArgsConstructor
public class BaseClientesFGTSController {
	
	private final BaseClientesFGTSService service;
	
	@ApiOperation(httpMethod = "POST", value = "Salvar base de clientes fgts", response = BaseClientesFGTSDto.class)
	@PostMapping
	public ResponseEntity<BaseClientesFGTSDto> salvarBaseClientesFGTS(@RequestBody BaseClientesFGTSForm formBaseClientesFgts) {
		return ResponseEntity.ok().body(service.salvarBaseClientesFGTS(formBaseClientesFgts));
	}
	
	@ApiOperation(httpMethod = "DELETE", value = "Deletar cliente da base", response = String.class)
	@DeleteMapping("/{uuidBaseClientesFGTS}")
	public ResponseEntity<Void> deletarBaseClientesFGTS(@PathVariable UUID uuidBaseClientesFGTS) {
		service.deletarBaseClientesFGTS(uuidBaseClientesFGTS);			
		return new ResponseEntity<Void>( HttpStatus.OK );
	}
	
	@ApiOperation(httpMethod = "PUT", value = "Alterar dados do cliente da base", response = BaseClientesFGTSDto.class)
	@PutMapping("/{uuidBaseClientesFGTS}/alterar-cliente")
	public ResponseEntity<BaseClientesFGTSDto> atualizarBaseClientesFGTS(@PathVariable UUID uuidBaseClientesFGTS, @RequestBody BaseClientesFGTSForm formBaseClientesFgts) {
		return ResponseEntity.ok().body(service.atualizarBaseClientesFGTS(uuidBaseClientesFGTS, formBaseClientesFgts));
	}

	@ApiOperation(httpMethod = "GET", value = "buscar clientes da base de fgts", response = BaseClientesFGTSDto[].class)
	@GetMapping
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "A pagina que você quer receber 0...n"),
		@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Número de registros por página."), })
	public ResponseEntity<List<BaseClientesFGTSDto>> listarBaseClientesFGTS(BaseClientesFGTSFilter filter, @ApiIgnore Pageable pageable) {
		RetornoListaBaseClientesFGTSDto baseClientes = service.listarBaseClientesFGTS(filter, pageable);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("totalRegistros", String.valueOf(baseClientes.getTotalElementos()));
		responseHeaders.set("Access-Control-Expose-Headers", "totalRegistros");

		return ResponseEntity.ok().headers(responseHeaders).body(baseClientes.getListaBaseClientesFgts());
	}

	@ApiOperation(httpMethod = "GET", value = "Exportar base de clientes FGTS para CSV")
	@GetMapping("/exportar-csv")
	public Resource exportarCsv(HttpServletResponse response, BaseClientesFGTSFilter criteria) {
		try {
			List<BaseClientesFGTSCsvDto> contratos = service.gerarCsvBaseClientesFGTS(criteria);
			String fileName = "baseClientesFgts.csv";

			// response config
			response.setContentType("text/csv");
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
			response.setLocale(new Locale("pt", "BR"));

			return ManipularCsv.escrever_UFT_Posicao(BaseClientesFGTSCsvDto.class, contratos);
		} catch (Exception e) {
			throw new BadRequestException(e.getMessage());
		}
	}
	
	@ApiOperation(httpMethod = "GET", value = "Exportar base de clientes FGTS para CSV - Enviar para gerenciador de arquivos")
	@GetMapping("/exportar-csv/gerenciador-arquivos")
	public ResponseEntity<Void> exportarCsvGerenciarArquivos() {
		service.buscarBase();
		return new ResponseEntity<Void>( HttpStatus.OK );
	}
		
}
