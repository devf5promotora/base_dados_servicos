package br.com.f5global.base_dados_servicos.controller;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.f5global.base_dados_servicos.exception.BadRequestException;
import br.com.f5global.base_dados_servicos.service.PreCadastroMarketPlaceService;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceCsvDTO;
import br.com.f5global.base_dados_servicos.service.dto.PreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.dto.RetornoListaPreCadastroMarketPlaceDTO;
import br.com.f5global.base_dados_servicos.service.filter.PreCadastroMarketPlaceFilter;
import br.com.f5global.base_dados_servicos.service.form.PreCadastroMarketPlaceForm;
import br.com.f5global.base_dados_servicos.service.impl.ManipularCsv;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/pre-cadastro-marketplace")
@RequiredArgsConstructor
public class PreCadastroMarketPlaceController {
	
	private final PreCadastroMarketPlaceService service;
	
	@ApiOperation(httpMethod = "POST", value = "Salvar base de parceiros fgts", response = PreCadastroMarketPlaceDTO.class)
	@PostMapping
	public ResponseEntity<PreCadastroMarketPlaceDTO> salvarPreCadastroMarketPlace(@RequestBody PreCadastroMarketPlaceForm formPreCadastroMarketPlace) {
		return ResponseEntity.ok().body(service.salvarPreCadastroMarketPlace(formPreCadastroMarketPlace));
	}
	
	@ApiOperation(httpMethod = "DELETE", value = "Deletar parceiro da base", response = String.class)
	@DeleteMapping("/{uuidPreCadastroMarketPlace}")
	public ResponseEntity<Void> deletarPreCadastroMarketPlace(@PathVariable UUID uuidPreCadastroMarketPlace) {
		service.deletarPreCadastroMarketPlace(uuidPreCadastroMarketPlace);			
		return new ResponseEntity<Void>( HttpStatus.OK );
	}
	
	@ApiOperation(httpMethod = "PUT", value = "Alterar dados do parceiro da base", response = PreCadastroMarketPlaceDTO.class)
	@PutMapping("/{uuidPreCadastroMarketPlace}/alterar-parceiro")
	public ResponseEntity<PreCadastroMarketPlaceDTO> atualizarPreCadastroMarketPlace(@PathVariable UUID uuidPreCadastroMarketPlace, @RequestBody PreCadastroMarketPlaceForm formPreCadastroMarketPlace) {
		return ResponseEntity.ok().body(service.atualizarPreCadastroMarketPlace(uuidPreCadastroMarketPlace, formPreCadastroMarketPlace));
	}

	@ApiOperation(httpMethod = "GET", value = "buscar parceiros da base de fgts", response = PreCadastroMarketPlaceDTO[].class)
	@GetMapping
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "A pagina que você quer receber 0...n"),
		@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Número de registros por página."), })
	public ResponseEntity<List<PreCadastroMarketPlaceDTO>> listarPreCadastroMarketPlace(PreCadastroMarketPlaceFilter filter, @ApiIgnore Pageable pageable) {
		RetornoListaPreCadastroMarketPlaceDTO baseparceiros = service.listarPreCadastroMarketPlace(filter, pageable);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("totalRegistros", String.valueOf(baseparceiros.getTotalElementos()));
		responseHeaders.set("Access-Control-Expose-Headers", "totalRegistros");

		return ResponseEntity.ok().headers(responseHeaders).body(baseparceiros.getListaPreCadastroMarketPlace());
	}

	@ApiOperation(httpMethod = "GET", value = "Exportar base de parceiros FGTS para CSV")
	@GetMapping("/exportar-csv")
	public Resource exportarCsv(HttpServletResponse response, PreCadastroMarketPlaceFilter criteria) {
		try {
			List<PreCadastroMarketPlaceCsvDTO> contratos = service.gerarCsvPreCadastroMarketPlace(criteria);
			String fileName = "PreCadastroMarketPlace.csv";

			// response config
			response.setContentType("text/csv");
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
			response.setLocale(new Locale("pt", "BR"));

			return ManipularCsv.escrever_UFT_Posicao(PreCadastroMarketPlaceCsvDTO.class, contratos);
		} catch (Exception e) {
			throw new BadRequestException(e.getMessage());
		}
	}
	
	@ApiOperation(httpMethod = "GET", value = "Exportar base de parceiros FGTS para CSV - Enviar para gerenciador de arquivos")
	@GetMapping("/exportar-csv/gerenciador-arquivos")
	public ResponseEntity<Void> exportarCsvGerenciarArquivos() {
		service.buscarBase();
		return new ResponseEntity<Void>( HttpStatus.OK );
	}
		
}
