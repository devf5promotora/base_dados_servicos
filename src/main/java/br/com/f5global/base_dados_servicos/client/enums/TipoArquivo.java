package br.com.f5global.base_dados_servicos.client.enums;

public enum TipoArquivo {
  DOCUMENTO,
  IMAGEM
}
