package br.com.f5global.base_dados_servicos.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import br.com.f5global.base_dados_servicos.client.dto.RetornoUploadDTO;
import br.com.f5global.base_dados_servicos.client.enums.TipoArquivo;
import br.com.f5global.base_dados_servicos.config.feign.MultipartSupportConfig;

@FeignClient(
		name = "IMPORTADOR",
		url = "http://34.121.65.22:3302/api/v1/importador",
		configuration = {MultipartSupportConfig.class})
public interface ServicoImportadorClient {
	@PostMapping(value = "/importar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	RetornoUploadDTO importarArquivo(
			@RequestPart MultipartFile arquivo,
			@RequestParam(name = "tipoArquivo") TipoArquivo tipoArquivo,
			@RequestHeader(value = "Authorization") String token);
}
