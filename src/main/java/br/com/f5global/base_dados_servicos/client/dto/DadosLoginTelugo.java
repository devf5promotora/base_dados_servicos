package br.com.f5global.base_dados_servicos.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
public class DadosLoginTelugo {

	@Value("${username}")
	private String username;
	@Value("${password}")
	private String password;

	public DadosLoginTelugo dadosLogin() {
		return DadosLoginTelugo.builder()
				.setUsername(this.username)
				.setPassword(this.password)
				.build();
	}
}
