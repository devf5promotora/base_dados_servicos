package br.com.f5global.base_dados_servicos.client.dto;

import java.time.LocalDateTime;
import java.util.UUID;

import br.com.f5global.base_dados_servicos.client.enums.NomeServico;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "set")
public class GerenciadorArquivoDTO {
  private UUID uuidGerenciadorArquivo;
  private LocalDateTime data;
  private String usuario;
  private NomeServico nomeServico;
  private String link;
}
