package br.com.f5global.base_dados_servicos.client;

import java.util.UUID;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.f5global.base_dados_servicos.client.dto.GerenciadorArquivoDTO;
import br.com.f5global.base_dados_servicos.client.enums.NomeServico;
import br.com.f5global.base_dados_servicos.config.feign.MultipartSupportConfig;

@FeignClient(
		name = "GERENCIADOR-ARQUIVO",
		url = "http://34.121.65.22:3302/api/v1/gerenciador-arquivo",
		configuration = {MultipartSupportConfig.class})
public interface GerenciadorArquivoClient {
	@PostMapping(value = "/gerenciador-arquivo")
	GerenciadorArquivoDTO cadastrar(@RequestHeader(value = "Authorization") String token,
			@RequestParam NomeServico nomeServico);

	@PutMapping(value = "/gerenciador-arquivo/{uuidGerenciadorArquivo}")
	GerenciadorArquivoDTO atualizarLink(@RequestHeader(value = "Authorization") String token,
			@PathVariable UUID uuidGerenciadorArquivo,
			@RequestParam String link);
	
	@PutMapping("/gerenciador-arquivo/{uuidGerenciadorArquivo}/usuario")
	  public ResponseEntity<GerenciadorArquivoDTO> atualizarUsuario(@RequestHeader(value = "Authorization") String token,
			  @PathVariable UUID uuidGerenciadorArquivo, @RequestParam String usuario);
}
