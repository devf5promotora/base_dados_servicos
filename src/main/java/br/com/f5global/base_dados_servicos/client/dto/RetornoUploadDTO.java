package br.com.f5global.base_dados_servicos.client.dto;

import br.com.f5global.base_dados_servicos.client.enums.TipoArquivo;
import lombok.Data;

@Data
public class RetornoUploadDTO {
  private String diretorioDoArquivo;
  private TipoArquivo tipoArquivo;
  private String empresaMaster;
  private String usuarioImportador;
}
