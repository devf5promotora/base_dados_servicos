package br.com.f5global.base_dados_servicos.client.enums;

public enum NomeServico {
  PROPOSTA,
  RECEBIMENTO,
  CADASTRO,
  BASE_FGTS,
  MARKETPLACE
}
