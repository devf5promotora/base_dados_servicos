package br.com.f5global.base_dados_servicos.client;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.f5global.base_dados_servicos.client.dto.Authorization;
import br.com.f5global.base_dados_servicos.client.dto.DadosLoginTelugo;

@FeignClient(
		name = "AUTENTICACAO" ,
		url = "http://34.121.65.22:3302/api/v1/autenticacao/login")
public interface AutenticacaoClient {

	@PostMapping
	public Authorization login(@RequestBody @Valid DadosLoginTelugo login); 
}
