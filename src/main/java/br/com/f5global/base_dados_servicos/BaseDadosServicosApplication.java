package br.com.f5global.base_dados_servicos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class BaseDadosServicosApplication {
	public static void main(String[] args) {
		SpringApplication.run(BaseDadosServicosApplication.class, args);
	}
}
